# Author: A Ghilardi
# Version: 1.0
# Date: 2015

rm(list=ls(all=TRUE))

textmsg<-"Maps and animations turned off by user"
write.csv(textmsg,paste("Out//",textmsg,".csv",sep=""),row.names = FALSE)


###############################
#########END OF SCRIPT#########
###############################